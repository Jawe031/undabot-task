# API Documentation

# Endpoint

* **Method:** GET
* **URL:** /api/v1/term-popularity/{provider}/search?term={term}

# Description
This JSON API endpoint provides information about the popularity of a given search term within a specified provider. 
The endpoint supports the GET method and expects two parameters: `{provider}` and `{term}`, both of which should be strings.

# Parameters
_`provider` (Path Parameter)_
* **Type**: String
* **Required**: Yes
* **Description**: Represents the provider for which term popularity is being queried. This parameter is a placeholder for the specific provider's identifier.

_`term` (Query Parameter)_
* **Type**: String
* **Required**: Yes
* **Description**: Represents the search term for which popularity information is requested. This parameter is a placeholder for the actual search term.

# Request & Response Examples

### **1. Successful request (valid provider and search term)**

`(GET) /api/v1/term-popularity/github/search?term=test`

```json
{
    "meta": {
        "message": "Popularity score successfully retrieved.",
        "http_code": 200,
        "method": "GET",
        "uri": "api/v1/term-popularity/github/search?term=test"
    },
    "errors": null,
    "data": {
        "type": "term_popularity_score",
        "id": 2,
        "attributes": {
            "term": "test",
            "score": 4.56,
            "provider": "github"
      }
  }
}
```

### **2. Invalid request (invalid/unsupported provider, but valid search term)**

`(GET) /api/v1/term-popularity/twitter/search?term=test`

```json
{
    "meta": {
        "message": "Invalid provider name: twitter.",
        "http_code": 400,
        "method": "GET",
        "uri": "api/v1/term-popularity/twitter/search?term=test"
    },
    "errors": {
        "status": "InvalidArgumentException",
        "code": "400",
        "title": "Bad Request: Invalid argument (invalid request payload).",
        "detail": "Invalid provider name: twitter."
    },
    "data": []
}
```

### **3. Invalid request (valid provider, but invalid/empty search term)**

`(GET) /api/v1/term-popularity/github/search?term=`

```json
{
    "meta": {
        "message": "Invalid or missing request parameters.",
        "http_code": 400,
        "method": "GET",
        "uri": "api/v1/term-popularity/twitter/search?term="
    },
    "errors": [
        {
            "status": "InvalidArgumentException",
            "code": "400",
            "title": "Bad Request: Invalid argument (invalid request payload).",
            "detail": "This value is too short. It should have 1 character or more."
        },
        {
            "status": "InvalidArgumentException",
            "code": "400",
            "title": "Bad Request: Invalid argument (invalid request payload).",
            "detail": "This value should not be blank."
        }
    ],
    "data": []
}
```

### **4. Invalid request (valid provider and search term, but invalid github API key)**

`(GET) /api/v1/term-popularity/github/search?term=app`

```json
{
    "meta": {
        "message": "HTTP/2 401  returned for \"https://api.github.com/search/issues?q=app%20rocks&per_page=1\".",
        "http_code": 401,
        "method": "GET",
        "uri": "api/v1/term-popularity/github/search?term=app"
    },
    "errors": {
        "status": "Symfony\\Component\\HttpClient\\Exception\\ClientException",
        "code": "401",
        "title": "Unauthorized.",
        "detail": "HTTP/2 401  returned for \"https://api.github.com/search/issues?q=app%20rocks&per_page=1\"."
    },
    "data": []
}
```
