<?php

namespace App\Controller\Api\V1;

use App\Http\DataMapper\Api\ExceptionDataMapper;
use App\Http\DataMapper\Api\TermPopularityScoreDataMapper;
use App\Http\Request\Api\TermPopularity\TermPopularitySearchRequest;
use App\Http\Trait\JsonResponseTrait;
use App\Service\TermPopularity\TermPopularityScoreService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/api/v1/term-popularity/{provider}', name: 'api_v1_term_popularity_')]
class TermPopularityApiV1Controller
{
    use JsonResponseTrait;

    /**
     * @param TermPopularityScoreService $popularityScoreService
     * @param TranslatorInterface $translator
     */
    public function __construct(
        protected readonly TermPopularityScoreService $popularityScoreService,
        protected readonly TranslatorInterface $translator
    )
    {
    }

    /**
     * @param TermPopularitySearchRequest $termPopularitySearchRequest
     * @param TermPopularityScoreDataMapper $termPopularityScoreDataMapper
     * @param ExceptionDataMapper $exceptionDataMapper
     * @param string $provider
     * @return JsonResponse
     */
    #[Route('/search', name: 'search', methods: 'GET')]
    public function search(
        TermPopularitySearchRequest $termPopularitySearchRequest,
        TermPopularityScoreDataMapper $termPopularityScoreDataMapper,
        ExceptionDataMapper $exceptionDataMapper,
        string $provider
    ): JsonResponse
    {
        $termPopularitySearchRequest->validate();

        $request = $termPopularitySearchRequest->getRequest();

        $term = $request->get('term');

        try {
            $termPopularityScore = $this->popularityScoreService->getPopularityScore(strtolower($term), $provider);
        } catch (\Exception $exception) {
            return $this->respond(
                $request,
                message: $exception->getMessage(),
                statusCode: $exception->getCode(),
                errors: ($exceptionDataMapper->createWithResource($exception))->toArray($request)
            );
        }

        return $this->respond(
            $request,
            ($termPopularityScoreDataMapper->createWithResource($termPopularityScore))->toArray($request),
            message: $this->translator->trans('api_search_success', domain: 'api_term_popularity')
        );
    }
}
