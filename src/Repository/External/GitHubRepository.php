<?php

namespace App\Repository\External;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GitHubRepository
{
    const string SEARCH_API_PREFIX = "https://api.github.com/search";

    /**
     * @param HttpClientInterface $httpClient
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly ParameterBagInterface $parameterBag
    )
    {
    }

    /**
     * @param array $queryParams
     * @return int
     */
    public function getIssuesTotalCountBy(array $queryParams): int
    {
        $response = $this->httpClient->request(
            'GET',
            self::SEARCH_API_PREFIX.'/issues',
            [
                'auth_bearer' => $this->parameterBag->get('github_v1_issues_api_key'),
                'query' => $queryParams
            ]
        );

        $result = $response->toArray();

        return $result['total_count'];
    }
}
