<?php

namespace App\Repository;

use App\Entity\TermPopularityScore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TermPopularityScore>
 *
 * @method TermPopularityScore|null find($id, $lockMode = null, $lockVersion = null)
 * @method TermPopularityScore|null findOneBy(array $criteria, array $orderBy = null)
 * @method TermPopularityScore[]    findAll()
 * @method TermPopularityScore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TermPopularityScoreRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        protected ManagerRegistry $registry,
        protected EntityManagerInterface $entityManager
    )
    {
        parent::__construct($registry, TermPopularityScore::class);
    }

    /**
     * Creates new term popularity score object
     * @param array $data
     * @return TermPopularityScore
     */
    public function create(array $data = []): TermPopularityScore
    {
        $termPopularityScore = new TermPopularityScore();
        $termPopularityScore->setTerm($data['term']);
        $termPopularityScore->setProvider($data['provider']);
        $termPopularityScore->setScore($data['score']);

        $this->entityManager->persist($termPopularityScore);

        $this->entityManager->flush();

        return $termPopularityScore;
    }
}
