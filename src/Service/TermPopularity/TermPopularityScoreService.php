<?php

namespace App\Service\TermPopularity;

use App\Contract\Service\TermPopularity\TermPopularityScoreProviderInterface;
use App\Entity\TermPopularityScore;
use Symfony\Contracts\Translation\TranslatorInterface;

class TermPopularityScoreService
{
    public function __construct(
        protected iterable $providers,
        protected DatabasePopularityScoreService $databasePopularityScoreService,
        protected TranslatorInterface $translator
    )
    {
    }

    /**
     * Finds requested provider if the term does not exist in database and returns the popularity score
     * @param string $term
     * @param string $providerName
     * @return TermPopularityScore
     */
    public function getPopularityScore(string $term, string $providerName): TermPopularityScore
    {
        $databasePopularityScore = $this->databasePopularityScoreService->getPopularityScore($term, $providerName);

        if ($databasePopularityScore !== null) {
            return $databasePopularityScore;
        }

        foreach ($this->providers as $provider) {
            if ($provider instanceof TermPopularityScoreProviderInterface && $provider->getName() === $providerName) {
                $popularityScore = $provider->getPopularityScore($term);

                return $this->databasePopularityScoreService->createPopularityScore($term, $providerName, $popularityScore);
            }
        }

        throw new \InvalidArgumentException(
            sprintf("%s: %s.",
                $this->translator->trans("api_exception_invalid_provider_name", domain: "api_exceptions"),
                $providerName
            ),
            400);
    }
}
