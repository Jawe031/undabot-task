<?php

namespace App\Service\TermPopularity;

class PopularityScoreCalculatorService
{
    /**
     * @param int $positiveResults
     * @param int $negativeResults
     * @return float
     */
    public function getScore(int $positiveResults, int $negativeResults): float
    {
        $positiveResults = max($positiveResults, 1);
        $negativeResults = max($negativeResults, 1);

        $ratio = $positiveResults / ($positiveResults + $negativeResults);

        return $this->mapToRange($ratio, 1, 10);
    }

    /**
     * @param float $value
     * @param float $min
     * @param float $max
     * @return float
     */
    private function mapToRange(float $value, float $min, float $max): float
    {
        $normalizedValue = max(0, min(1, $value));

        return round($min + $normalizedValue * ($max - $min), 2);
    }
}
