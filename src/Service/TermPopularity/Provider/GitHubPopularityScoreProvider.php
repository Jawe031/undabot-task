<?php

namespace App\Service\TermPopularity\Provider;

use App\Contract\Service\TermPopularity\TermPopularityScoreProviderInterface;
use App\Repository\External\GitHubRepository;
use App\Service\TermPopularity\PopularityScoreCalculatorService;

class GitHubPopularityScoreProvider implements TermPopularityScoreProviderInterface
{
    const string PROVIDER_NAME = 'github';

    /**
     * @param GitHubRepository $gitHubRepository
     * @param PopularityScoreCalculatorService $popularityScoreCalculatorService
     */
    public function __construct(
        protected GitHubRepository $gitHubRepository,
        private readonly PopularityScoreCalculatorService $popularityScoreCalculatorService
    )
    {
    }

    /**
     * @param string $term
     * @return float
     */
    public function getPopularityScore(string $term): float
    {
        $positiveResults = $this->gitHubRepository->getIssuesTotalCountBy([
            'q' => sprintf(
                "%s %s",
                $term,
                TermPopularityScoreProviderInterface::POSITIVE_RESULTS_SUFFIX
            ),
            'per_page' => 1
        ]);

        $negativeResults = $this->gitHubRepository->getIssuesTotalCountBy([
            'q' => sprintf(
                "%s %s",
                $term,
                TermPopularityScoreProviderInterface::NEGATIVE_RESULTS_SUFFIX
            ),
            'per_page' => 1
        ]);

        return $this->popularityScoreCalculatorService->getScore($positiveResults, $negativeResults);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::PROVIDER_NAME;
    }
}
