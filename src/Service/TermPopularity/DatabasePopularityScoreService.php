<?php

namespace App\Service\TermPopularity;

use App\Entity\TermPopularityScore;
use App\Repository\TermPopularityScoreRepository;

class DatabasePopularityScoreService
{
    /**
     * @param TermPopularityScoreRepository $termPopularityScoreRepository
     */
    public function __construct(
        protected TermPopularityScoreRepository $termPopularityScoreRepository
    )
    {
    }

    /**
     * @param string $term
     * @param string $provider
     * @return null|TermPopularityScore
     */
    public function getPopularityScore(string $term, string $provider): null|TermPopularityScore
    {
        $result = $this->termPopularityScoreRepository->findOneBy([
            'term' => $term,
            'provider' => $provider
        ]);

        if ($result instanceof TermPopularityScore) {
            return $result;
        }

        return null;
    }

    /**
     * @param string $term
     * @param string $provider
     * @param float $score
     * @return TermPopularityScore
     */
    public function createPopularityScore(string $term, string $provider, float $score): TermPopularityScore
    {
        return $this->termPopularityScoreRepository->create([
            'term' => $term,
            'provider' => $provider,
            'score' => $score
        ]);
    }
}
