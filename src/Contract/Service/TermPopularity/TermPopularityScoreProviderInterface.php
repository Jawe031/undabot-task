<?php

namespace App\Contract\Service\TermPopularity;

interface TermPopularityScoreProviderInterface
{
    const string POSITIVE_RESULTS_SUFFIX = 'rocks';

    const string NEGATIVE_RESULTS_SUFFIX = 'sucks';

    /**
     * @param string $term
     * @return float
     */
    public function getPopularityScore(string $term): float;

    /**
     * @return string
     */
    public function getName(): string;
}
