<?php

namespace App\Http\Trait;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

trait JsonResponseTrait
{
    /**
     * @param ContainerInterface $container
     */
    public function __construct(
        private readonly ContainerInterface $container
    )
    {
    }

    /**
     * @param Request $request
     * @param array $data
     * @param string $message
     * @param int $statusCode
     * @param null $errors
     * @param array $headers
     * @return JsonResponse
     */
    public function respond(
        Request $request,
        array $data = [],
        string $message = 'Success',
        int $statusCode = 200,
        $errors = null,
        array $headers = []
    ): JsonResponse
    {
        $response = [
            "meta"   => [
                "message"   => $message,
                "http_code" => $statusCode,
                "method"    => $request->getMethod(),
                "uri"       => ltrim($request->getRequestUri(), '/')
            ],
            "errors" => $errors,
            "data"   => $data
        ];

        return new JsonResponse($response, $statusCode, $headers);
    }

    /**
     * @param array $data
     * @param int $status
     * @param array $headers
     * @param array $context
     * @return JsonResponse
     */
    protected function json(array $data, int $status = 200, array $headers = [], array $context = []): JsonResponse
    {
        if ($this->container->has('serializer')) {
            $json = $this->container->get('serializer')->serialize($data, 'json', array_merge([
                'json_encode_options' => JsonResponse::DEFAULT_ENCODING_OPTIONS,
            ], $context));

            return new JsonResponse($json, $status, $headers, true);
        }

        return new JsonResponse($data, $status, $headers);
    }
}
