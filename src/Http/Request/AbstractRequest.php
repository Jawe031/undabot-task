<?php

namespace App\Http\Request;

use App\Http\DataMapper\Api\ExceptionDataMapper;
use App\Http\Trait\JsonResponseTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractRequest
{
    use JsonResponseTrait;

    /**
     * @param ValidatorInterface $validator
     * @param TranslatorInterface $translator
     * @param ExceptionDataMapper $exceptionDataMapper
     */
    public function __construct(
        protected ValidatorInterface $validator,
        protected TranslatorInterface $translator,
        protected ExceptionDataMapper $exceptionDataMapper
    )
    {
        $this->populate();
    }

    /**
     * @return void
     */
    public function validate(): void
    {
        $errors = $this->validator->validate($this);

        $validationExceptions = [];

        /** @var ConstraintViolation $message */
        foreach ($errors as $message) {
            $validationExceptions[] = new \InvalidArgumentException(
                $message->getMessage(), 400
            );
        }

        if (count($validationExceptions) > 0) {
            $response = $this->respond(
                $this->getRequest(),
                message: $this->translator->trans('api_exception_validation_error', domain: 'api_exceptions'),
                statusCode: 400,
                errors: $this->exceptionDataMapper::list($validationExceptions, $this->translator)
                    ->toArray($this->getRequest())
            );
            $response->send();

            exit;
        }
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return Request::createFromGlobals();
    }

    /**
     * @return void
     */
    protected function populate(): void
    {
        $properties = array_merge(
            $this->getRequest()->request->all(),
            $this->getRequest()->attributes->all(),
            $this->getRequest()->query->all()
        );

        foreach ($properties as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }
    }
}
