<?php

namespace App\Http\Request\Api\TermPopularity;

use App\Http\Request\AbstractRequest;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class TermPopularitySearchRequest extends AbstractRequest
{
    #[Type('string')]
    #[Length(min: 1, max: 255)]
    #[NotBlank()]
    protected string $term;
}
