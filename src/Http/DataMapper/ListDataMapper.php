<?php

namespace App\Http\DataMapper;

use Symfony\Component\HttpFoundation\Request;

final class ListDataMapper extends AbstractDataMapper
{
    /**
     * @param mixed $list
     * @param string $className
     * @param array $arguments
     */
    public function __construct(
        protected mixed $list,
        protected string $className,
        protected array $arguments = []
    )
    {
        parent::__construct($list);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function toArray(Request $request): array
    {
        return $this->all($request);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function all(Request $request): array
    {
        $list = [];

        foreach ($this->resource as $data) {
            $list[] = (new $this->className($data, ...$this->arguments))->all($request);
        }

        return $list;
    }
}
