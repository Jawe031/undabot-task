<?php

namespace App\Http\DataMapper;

use Symfony\Component\HttpFoundation\Request;

abstract class AbstractDataMapper
{
    /**
     * @param mixed $resource
     */
    public function __construct(
        protected mixed $resource
    )
    {
    }

    /**
     * @param Request $request
     * @return array
     */
    abstract public function toArray(Request $request): array;

    /**
     * @param Request $request
     * @return null|array
     */
    public function all(Request $request): ?array
    {
        if (empty($this->resource)) {
            return $this->resource;
        }

        $result = [];
        $data = $this->toArray($request);

        if (empty($data)) {
            return $data;
        }

        foreach ($data as $fieldName => $value) {
            if ($value instanceof self) {
                $result[$fieldName] = $value->all($request);
                continue;
            }

            $result[$fieldName] = $value;
        }

        return $result;
    }

    /**
     * @param array $list
     * @return ListDataMapper
     */
    public static function list(array $list): ListDataMapper
    {
        $arguments = func_get_args();

        array_shift($arguments);

        return new ListDataMapper($list, static::class, $arguments);
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call(string $name, array $arguments): mixed
    {
        return call_user_func_array([$this->resource, $name], $arguments);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return $this->resource->{$name};
    }
}
