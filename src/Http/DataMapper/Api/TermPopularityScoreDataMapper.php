<?php

namespace App\Http\DataMapper\Api;

use App\Entity\TermPopularityScore;
use Symfony\Component\HttpFoundation\Request;

/**
 * @property TermPopularityScore $resource
 */
class TermPopularityScoreDataMapper extends EntityDataMapper
{
    /**
     * @param Request $request
     * @return array
     */
    #[\Override] public function toArray(Request $request): array
    {
        return [
            "type" => $this->entityManager->getClassMetadata(TermPopularityScore::class)->getTableName(),
            "id" => $this->resource->getId(),
            "attributes" => [
                "term" => $this->resource->getTerm(),
                "score" => (float) $this->resource->getScore(),
                "provider" => $this->resource->getProvider()
            ]
        ];
    }
}
