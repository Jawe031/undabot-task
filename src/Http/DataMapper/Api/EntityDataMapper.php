<?php

namespace App\Http\DataMapper\Api;

use App\Http\DataMapper\AbstractDataMapper;
use Doctrine\ORM\EntityManagerInterface;

abstract class EntityDataMapper extends AbstractDataMapper
{
    /**
     * @param mixed $resource
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        protected mixed                  $resource,
        protected EntityManagerInterface $entityManager
    )
    {
        parent::__construct($resource);
    }

    /**
     * @param mixed $resource
     * @return static
     */
    public function createWithResource(mixed $resource): static
    {
        return new static($resource, $this->entityManager);
    }
}
