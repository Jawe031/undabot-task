<?php

namespace App\Http\DataMapper\Api;

use App\Http\DataMapper\AbstractDataMapper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @property \Exception $resource
 */
class ExceptionDataMapper extends AbstractDataMapper
{
    /**
     * @param mixed $resource
     * @param TranslatorInterface $translator
     */
    public function __construct(
        protected mixed $resource,
        protected TranslatorInterface $translator
    )
    {
        parent::__construct($resource);
    }

    /**
     * @param Request $request
     * @return array
     */
    #[\Override] public function toArray(Request $request): array
    {
        return [
            "status" => get_class($this->resource),
            "code" => (string) $this->resource->getCode(),
            "title" => $this->translator->trans("api_exception_{$this->resource->getCode()}_title", domain: 'api_exceptions'),
            "detail" => $this->resource->getMessage()
        ];
    }

    /**
     * @param mixed $resource
     * @return static
     */
    public function createWithResource(mixed $resource): static
    {
        return new static($resource, $this->translator);
    }
}
