<?php

namespace App\Tests\Unit\Http\DataMapper\Api;

use App\Entity\TermPopularityScore;
use App\Http\DataMapper\Api\TermPopularityScoreDataMapper;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class TermPopularityScoreDataMapperTest extends TestCase
{
    /**
     * @return void
     */
    public function testListMethod(): void
    {
        $data = [];
        $expectedResult = [];

        for ($i=1; $i <= 5; $i++) {
            $score = (string)($i+1);
            $provider = "github";
            $term = "php $i";

            $termPopularityScore = $this->createMock(TermPopularityScore::class);
            $termPopularityScore->method('getId')->willReturn($i);
            $termPopularityScore->method('getTerm')->willReturn($term);
            $termPopularityScore->method('getScore')->willReturn($score);
            $termPopularityScore->method('getProvider')->willReturn($provider);

            $data[] = $termPopularityScore;

            $expectedResult[] = [
                "type" => 'term_popularity_scores',
                "id" => $i,
                "attributes" => [
                    "term" => $term,
                    "score" => (float) $score,
                    "provider" => $provider
                ]
            ];
        }

        $entityManager = $this->createMock(EntityManager::class);
        $classMetadata = $this->createMock(ClassMetadata::class);
        $classMetadata->method('getTableName')->willReturn('term_popularity_scores');
        $entityManager->method('getClassMetadata')->willReturn($classMetadata);

        $dataMapper = TermPopularityScoreDataMapper::list($data, $entityManager);
        $request = $this->createMock(Request::class);

        $result = $dataMapper->toArray($request);

        $this->assertIsArray($result);
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return void
     */
    public function testToArrayMethod(): void
    {
        $termPopularityScore = $this->createMock(TermPopularityScore::class);
        $termPopularityScore->method('getId')->willReturn(1);
        $termPopularityScore->method('getTerm')->willReturn('php');
        $termPopularityScore->method('getScore')->willReturn("42.0");
        $termPopularityScore->method('getProvider')->willReturn('github');

        $entityManager = $this->createMock(EntityManager::class);
        $classMetadata = $this->createMock(ClassMetadata::class);
        $classMetadata->method('getTableName')->willReturn('term_popularity_scores');
        $entityManager->method('getClassMetadata')->willReturn($classMetadata);

        $dataMapper = new TermPopularityScoreDataMapper($termPopularityScore, $entityManager);

        $request = $this->createMock(Request::class);

        $result = $dataMapper->toArray($request);

        $this->assertIsArray($result);

        $expectedResult = [
            "type" => 'term_popularity_scores',
            "id" => 1,
            "attributes" => [
                "term" => 'php',
                "score" => 42.0,
                "provider" => 'github'
            ]
        ];

        $this->assertEquals($expectedResult, $result);
    }
}
