<?php

namespace App\Tests\Unit\Http\DataMapper\Api;

use App\Http\DataMapper\Api\ExceptionDataMapper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExceptionDataMapperTest extends TestCase
{
    /**
     * @var TranslatorInterface
     */
    private TranslatorInterface $translator;

    /**
     * @var Request
     */
    private Request $request;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->translator->method('trans')->willReturn('Translated exception');
        $this->request = $this->createMock(Request::class);
    }

    /**
     * @return void
     */
    public function testListMethod(): void
    {
        $data = [];
        $expectedResult = [];

        for ($i=1; $i <= 5; $i++) {
            $data[] = new \Exception("Exception $i", 400);

            $expectedResult[] = [
                "status" => "Exception",
                "code" => '400',
                "title" => 'Translated exception',
                "detail" => "Exception $i"
            ];
        }

        $dataMapper = ExceptionDataMapper::list($data, $this->translator);
        $result = $dataMapper->toArray($this->request);

        $this->assertIsArray($result);
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return void
     */
    public function testToArrayMethod(): void
    {
        $exception = new \Exception('Exception', 400);

        $dataMapper = new ExceptionDataMapper($exception, $this->translator);

        $result = $dataMapper->toArray($this->request);

        $this->assertIsArray($result);

        $expectedResult = [
            "status" => 'Exception',
            "code" => '400',
            "title" => 'Translated exception',
            "detail" => 'Exception'
        ];

        $this->assertEquals($expectedResult, $result);
    }
}
