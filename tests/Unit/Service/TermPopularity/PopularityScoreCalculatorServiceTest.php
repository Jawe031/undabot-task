<?php

namespace App\Tests\Unit\Service\TermPopularity;

use App\Service\TermPopularity\PopularityScoreCalculatorService;
use PHPUnit\Framework\TestCase;

class PopularityScoreCalculatorServiceTest extends TestCase
{
    /**
     * @return void
     */
    public function testGetScore(): void
    {
        $calculator = new PopularityScoreCalculatorService();

        $positiveResults = 10;
        $negativeResults = 5;

        $expectedScore = $calculator->getScore($positiveResults, $negativeResults);

        $this->assertGreaterThanOrEqual(1, $expectedScore);
        $this->assertLessThanOrEqual(10, $expectedScore);
    }
}
