<?php

namespace App\Tests\Unit\Service\TermPopularity\Provider;

use App\Contract\Service\TermPopularity\TermPopularityScoreProviderInterface;
use App\Repository\External\GitHubRepository;
use App\Service\TermPopularity\Provider\GitHubPopularityScoreProvider;
use App\Service\TermPopularity\PopularityScoreCalculatorService;
use PHPUnit\Framework\TestCase;

class GitHubPopularityScoreProviderTest extends TestCase
{
    /**
     * @var GitHubRepository
     */
    private GitHubRepository $gitHubRepository;

    /**
     * @var PopularityScoreCalculatorService
     */
    private PopularityScoreCalculatorService $popularityScoreCalculatorService;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->gitHubRepository = $this->createMock(GitHubRepository::class);
        $this->popularityScoreCalculatorService = $this->createMock(PopularityScoreCalculatorService::class);
    }

    /**
     * @return void
     */
    public function testGetPopularityScore(): void
    {
        $term = 'php';
        $positiveResults = 10;
        $negativeResults = 10;
        $expectedScore = 5.0;

        $this->gitHubRepository->method('getIssuesTotalCountBy')
            ->willReturnCallback(function ($args) use ($term, &$positiveResults, &$negativeResults) {
                $termWithPositiveSuffix = "$term ".TermPopularityScoreProviderInterface::POSITIVE_RESULTS_SUFFIX;

                if ($args['q'] === $termWithPositiveSuffix) {
                    return $positiveResults;
                }

                return $negativeResults;
            });

        $this->popularityScoreCalculatorService->method('getScore')
            ->willReturnCallback(function ($posResults, $negResults) use (&$positiveResults, &$negativeResults, $expectedScore) {
                $this->assertSame($positiveResults, $posResults);
                $this->assertSame($negativeResults, $negResults);
                return $expectedScore;
            });

        $this->popularityScoreCalculatorService->expects($this->once())
            ->method('getScore')
            ->with($positiveResults, $negativeResults)
            ->willReturn($expectedScore);

        $provider = new GitHubPopularityScoreProvider($this->gitHubRepository, $this->popularityScoreCalculatorService);

        $result = $provider->getPopularityScore($term);

        $this->assertEquals($expectedScore, $result);
    }

    /**
     * @return void
     */
    public function testGetName(): void
    {
        $provider = new GitHubPopularityScoreProvider($this->gitHubRepository, $this->popularityScoreCalculatorService);

        $result = $provider->getName();

        $this->assertEquals(GitHubPopularityScoreProvider::PROVIDER_NAME, $result);
    }
}
