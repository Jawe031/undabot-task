<?php

namespace App\Tests\Unit\Service\TermPopularity;

use App\Entity\TermPopularityScore;
use App\Repository\TermPopularityScoreRepository;
use App\Service\TermPopularity\DatabasePopularityScoreService;
use PHPUnit\Framework\TestCase;

class DatabasePopularityScoreServiceTest extends TestCase
{
    /**
     * @var TermPopularityScoreRepository
     */
    private TermPopularityScoreRepository $termPopularityScoreRepository;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->termPopularityScoreRepository = $this->createMock(TermPopularityScoreRepository::class);
    }

    /**
     * @return void
     */
    public function testGetPopularityScoreReturnsNullIfNotFound(): void
    {
        $this->termPopularityScoreRepository->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $service = new DatabasePopularityScoreService($this->termPopularityScoreRepository);

        $result = $service->getPopularityScore('nonexistent_term', 'nonexistent_provider');

        $this->assertNull($result);
    }

    /**
     * @return void
     */
    public function testGetPopularityScoreReturnsTermPopularityScoreIfFound(): void
    {
        $expectedTermPopularityScore = new TermPopularityScore();
        $this->termPopularityScoreRepository->expects($this->once())
            ->method('findOneBy')
            ->willReturn($expectedTermPopularityScore);

        $service = new DatabasePopularityScoreService($this->termPopularityScoreRepository);

        $result = $service->getPopularityScore('php', 'github');

        $this->assertSame($expectedTermPopularityScore, $result);
    }

    /**
     * @return void
     */
    public function testCreatePopularityScore(): void
    {
        $expectedTermPopularityScore = new TermPopularityScore();
        $this->termPopularityScoreRepository->expects($this->once())
            ->method('create')
            ->with(['term' => 'php', 'provider' => 'github', 'score' => 5.0])
            ->willReturn($expectedTermPopularityScore);

        $service = new DatabasePopularityScoreService($this->termPopularityScoreRepository);

        $result = $service->createPopularityScore('php', 'github', 5.0);

        $this->assertSame($expectedTermPopularityScore, $result);
    }
}
