<?php

namespace App\Tests\Unit\Service\TermPopularity;

use App\Contract\Service\TermPopularity\TermPopularityScoreProviderInterface;
use App\Entity\TermPopularityScore;
use App\Service\TermPopularity\DatabasePopularityScoreService;
use App\Service\TermPopularity\TermPopularityScoreService;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class TermPopularityScoreServiceTest extends TestCase
{
    /**
     * @return void
     */
    public function testGetPopularityScoreFromDatabase(): void
    {
        $term = 'php';
        $providerName = 'github';

        $databasePopularityScoreService = $this->createMock(DatabasePopularityScoreService::class);
        $databasePopularityScoreService->expects($this->once())
            ->method('getPopularityScore')
            ->with($term, $providerName)
            ->willReturn(new TermPopularityScore());

        $service = new TermPopularityScoreService(
            [],
            $databasePopularityScoreService,
            $this->createMock(TranslatorInterface::class)
        );

        $result = $service->getPopularityScore($term, $providerName);

        $this->assertInstanceOf(TermPopularityScore::class, $result);
    }

    /**
     * @return void
     */
    public function testGetPopularityScoreFromProvider(): void
    {
        $term = 'nonexistent_term';
        $providerName = 'github';
        $popularityScoreValue = 5.0;

        $termPopularityScore = new TermPopularityScore();
        $termPopularityScore->setScore("5.0");

        $databasePopularityScoreService = $this->createMock(DatabasePopularityScoreService::class);
        $databasePopularityScoreService->expects($this->once())
            ->method('getPopularityScore')
            ->with($term, $providerName)
            ->willReturn($termPopularityScore);

        $service = new TermPopularityScoreService(
            [$this->createMock(TermPopularityScoreProviderInterface::class)],
            $databasePopularityScoreService,
            $this->createMock(TranslatorInterface::class)
        );

        $result = $service->getPopularityScore($term, $providerName);

        $this->assertInstanceOf(TermPopularityScore::class, $result);
        $this->assertSame($popularityScoreValue, (float)$result->getScore());
    }

    /**
     * @return void
     */
    public function testInvalidProviderNameThrowsException(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $term = 'php';
        $invalidProviderName = 'twitter';

        $databasePopularityScoreService = $this->createMock(DatabasePopularityScoreService::class);
        $databasePopularityScoreService->expects($this->once())
            ->method('getPopularityScore')
            ->with($term, $invalidProviderName)
            ->willReturn(null);

        $providerMock = $this->createMock(TermPopularityScoreProviderInterface::class);
        $providerMock->expects($this->once())
            ->method('getName')
            ->willReturn('github');

        $service = new TermPopularityScoreService(
            [$providerMock],
            $databasePopularityScoreService,
            $this->createMock(TranslatorInterface::class)
        );

        $service->getPopularityScore($term, $invalidProviderName);
    }
}
