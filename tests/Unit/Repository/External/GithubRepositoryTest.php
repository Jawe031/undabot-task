<?php

namespace App\Tests\Unit\Repository\External;

use App\Repository\External\GitHubRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class GithubRepositoryTest extends TestCase
{
    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $httpClientMock;

    /**
     * @var ParameterBagInterface
     */
    private ParameterBagInterface $parameterBagMock;

    /**
     * @var GitHubRepository
     */
    private GitHubRepository $gitHubRepository;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->httpClientMock = $this->createMock(HttpClientInterface::class);
        $this->parameterBagMock = $this->createMock(ParameterBagInterface::class);
        $this->gitHubRepository = new GitHubRepository($this->httpClientMock, $this->parameterBagMock);
    }

    /**
     * @return void
     */
    public function testGetIssuesTotalCountBy(): void
    {
        $queryParams = ['term' => 'bug'];
        $apiKey = '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';
        $expectedTotalCount = 42;

        $this->parameterBagMock
            ->expects($this->once())
            ->method('get')
            ->with('github_v1_issues_api_key')
            ->willReturn($apiKey);

        $responseMock = $this->createMock(ResponseInterface::class);
        $responseMock
            ->expects($this->once())
            ->method('toArray')
            ->willReturn(['total_count' => $expectedTotalCount]);

        $this->httpClientMock
            ->expects($this->once())
            ->method('request')
            ->with(
                'GET',
                GitHubRepository::SEARCH_API_PREFIX . '/issues',
                [
                    'auth_bearer' => $apiKey,
                    'query' => $queryParams,
                ]
            )
            ->willReturn($responseMock);

        $result = $this->gitHubRepository->getIssuesTotalCountBy($queryParams);

        $this->assertSame($expectedTotalCount, $result);
    }
}
