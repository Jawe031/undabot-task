<?php

namespace App\Tests\Unit\Repository;

use App\Repository\TermPopularityScoreRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Bridge\Doctrine\ManagerRegistry;

class TermPopularityScoreRepositoryTest extends TestCase
{
    /**
     * @var TermPopularityScoreRepository
     */
    private TermPopularityScoreRepository $termPopularityScoreRepository;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->termPopularityScoreRepository = new TermPopularityScoreRepository(
            $this->createMock(ManagerRegistry::class),
            $this->createMock(EntityManagerInterface::class)
        );
    }

    /**
     * @return void
     */
    public function testCreate(): void
    {
        $data = [
            'term' => 'php',
            'provider' => 'github',
            'score' => "42",
        ];

        $termPopularityScore = $this->termPopularityScoreRepository->create($data);

        $this->assertSame($data['term'], $termPopularityScore->getTerm());
        $this->assertSame($data['provider'], $termPopularityScore->getProvider());
        $this->assertSame($data['score'], $termPopularityScore->getScore());
    }
}
