<?php

namespace App\Tests\Feature\Controller\Api\V1;

use App\Controller\Api\V1\TermPopularityApiV1Controller;
use App\Entity\TermPopularityScore;
use App\Http\DataMapper\Api\ExceptionDataMapper;
use App\Http\DataMapper\Api\TermPopularityScoreDataMapper;
use App\Http\Request\Api\TermPopularity\TermPopularitySearchRequest;
use App\Service\TermPopularity\TermPopularityScoreService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class TermPopularityApiV1ControllerTest extends TestCase
{
    /**
     * @return void
     */
    public function testSearch(): void
    {
        $termPopularitySearchRequest = $this->createMock(TermPopularitySearchRequest::class);
        $termPopularityScoreDataMapper = $this->createMock(TermPopularityScoreDataMapper::class);
        $exceptionDataMapper = $this->createMock(ExceptionDataMapper::class);
        $translator = $this->createMock(TranslatorInterface::class);

        $popularityScoreService = $this->createMock(TermPopularityScoreService::class);

        $provider = 'github';
        $term = 'php';

        $request = new Request(['term' => $term]);

        $termPopularitySearchRequest->expects($this->once())
            ->method('validate');
        $termPopularitySearchRequest->expects($this->once())
            ->method('getRequest')
            ->willReturn($request);

        $termPopularityScore = $this->createMock(TermPopularityScore::class);

        $popularityScoreService->expects($this->once())
            ->method('getPopularityScore')
            ->with(strtolower($term), $provider)
            ->willReturn($termPopularityScore);

        $termPopularityScoreDataMapper->expects($this->once())
            ->method('createWithResource')
            ->with($termPopularityScore)
            ->willReturn($this->createMock(TermPopularityScoreDataMapper::class));

        $controller = new TermPopularityApiV1Controller(
            $popularityScoreService,
            $translator
        );

        $result = $controller->search(
            $termPopularitySearchRequest,
            $termPopularityScoreDataMapper,
            $exceptionDataMapper,
            $provider
        );

        $this->assertInstanceOf(JsonResponse::class, $result);

        $data = json_decode($result->getContent(), true);

        $this->assertIsArray($data);
        $this->assertArrayHasKey("meta", $data);
        $this->assertArrayHasKey("errors", $data);
        $this->assertArrayHasKey("data", $data);
    }
}
